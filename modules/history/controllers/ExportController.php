<?php
namespace app\modules\history\controllers;

use app\controllers\SiteController;
use app\modules\history\models\ExportForm;
use app\modules\history\models\search\HistorySearch;
use Yii;
use yii\helpers\Url;


/**
 * Class ExportController
 * @package modules\history\controllers
 */
class ExportController extends SiteController
{
    const CACHE_TIME = 600;
    /**
     * @var int
     */
    private $batchSize = 2000;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::class,
            'only' => ['index'],
            'rules' => [
                [
                    'allow' => true,
                    'verbs' => ['GET'],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // Лучше всего выносить генерацию файла в крон. Это потенциально опасный код - 1ая строка заставит скрипт работать пока не закончит,
        // 2ая съедает много памяти, если импорт делать будет много людей одновременно - может не хватить памяти на сервере
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');

        return parent::beforeAction($action);
    }

    /**
     * Export history to csv file
     *
     * @param $exportType
     * @return string
     */
    public function actionIndex($exportType)
    {

        $model = new HistorySearch();

        $cacheKey = md5(Url::to());
        $cache = Yii::$app->cache;
        $dataProvider = $cache->get($cacheKey);
        //Если данных нет в кеше - кешируем
        if (!$dataProvider) {
            $dataProvider = $model->search(\Yii::$app->request->queryParams);
            $cache->set($cacheKey, $dataProvider, self::CACHE_TIME);
        }

        return $this->render('export', [
            'dataProvider' => $dataProvider,
            'exportType' => $exportType,
            'batchSize' => $this->batchSize,
            'filename' => $this->generateFileName()
        ]);
    }

    /**
     * @return string
     */
    private function generateFileName()
    {
        return 'history-' . time();
    }
}