<?php
namespace app\modules\history\controllers;

use yii\filters\AccessControl;
use app\controllers\SiteController;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class HistoryController
 * @package modules\history\controllers
 */
class HistoryController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::class,
            'only' => ['index'],
            'rules' => [
                [
                    'allow' => true,
                    'verbs' => ['GET'],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Displays history list.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
