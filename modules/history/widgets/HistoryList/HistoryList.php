<?php

namespace app\modules\history\widgets\HistoryList;

use app\modules\history\models\search\HistorySearch;
use app\widgets\Export\Export;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

class HistoryList extends Widget
{
    public function run()
    {
        $model = new HistorySearch();

        return $this->render('main', [
            'model' => $model,
            'linkExport' => $this->getLinkExport(),
            'dataProvider' => $model->search(\Yii::$app->request->queryParams)
        ]);
    }

    /**
     * @return string
     */
    private function getLinkExport()
    {
        // Т.к. параметр page в GET не влияет на количество записей в импорте (выгружается все) - можно опустить получение параметров в адресной строке
        return Url::to(['/history/export', 'exportType' =>  Export::FORMAT_CSV]);
    }
}
