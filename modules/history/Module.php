<?php
namespace app\modules\history;

/**
 * Class Module
 * @package modules\history
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\history\controllers';

    public $controllerMap = [
        'history' => 'app\modules\history\controllers\HistoryController',
    ];

}
